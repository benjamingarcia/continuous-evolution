package downloader

import (
	"continuous-evolution/src/project"
	"continuous-evolution/src/project/io"
	"errors"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

//LocalConfig is configuration required by local downloader
type LocalConfig struct {
	Enabled bool
}

//DefaultLocalConfig is the default configuration required by local downloader
var DefaultLocalConfig = LocalConfig{Enabled: true}

var loggerLocal = logrus.WithField("logger", "downloader/local")

type local struct{ config Config }

func newLocal(config Config) downloader {
	return local{config}
}

func (l local) accept(url string) bool {
	isSlashPrefixed := strings.HasPrefix(url, "/")
	isSlashSuffixed := strings.HasSuffix(url, "/")
	pathExist := io.PathExists(url)
	isGitPath := io.PathExists(url + "/.git")
	loggerLocal.
		WithField("isSlashPrefixed", isSlashPrefixed).
		WithField("isSlashSuffixed", isSlashSuffixed).
		WithField("pathExist", pathExist).
		WithField("isGitPath", isGitPath).
		Debug("Local downloader")
	return isSlashPrefixed && isSlashSuffixed && pathExist && isGitPath
}

func (l local) buildProject(url string) (project.Project, error) {
	path := strings.Split(url, "/")
	name := path[len(path)-1]
	rest := strings.Join(path[:len(path)-1], "/")

	loggerLocal.WithFields(logrus.Fields{
		"path":        url,
		"PathToWrite": rest,
		"projectName": name,
	}).Info("New project")

	return project.Project{
		CreatedDate:     time.Now(),
		LastUpdatedDate: time.Now(),
		Name:            name,
		GitURL:          url,
		BranchName:      l.config.BranchName,
		PathToWrite:     rest,
		Login:           "",
		Token:           "",
		TypeHost:        "local",
		Organisation:    "",
	}, nil
}

func (l local) download(project project.Project) (project.Project, error) {
	//no need to download only check if git state is clean
	if project.Git().Diff() {
		return project, errors.New("git working dir is not clean")
	}
	branchAlreadyExist, err := project.Git().BranchAlreadyExist()
	if err != nil {
		loggerGit.WithField("project", project).WithError(err).Error("Can't get if merge request already exist")
		return project, err
	}
	return project.SetBranchAlreadyExist(branchAlreadyExist), nil
}
