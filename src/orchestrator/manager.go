package orchestrator

import (
	"continuous-evolution/src/deleter"
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/project"
	"errors"
	"fmt"
	"os"
	"sync"

	"github.com/sirupsen/logrus"
)

var loggerManager = logrus.WithField("logger", "orchestrator/manager")

//Config is configuration required by orchestrator
type Config struct {
	Web WebConfig
	Cli CliConfig
}

//DefaultConfig is the default configuration required by orchestrator
var DefaultConfig = Config{Web: DefaultWebConfig, Cli: DefaultCliConfig}

type manager struct {
	sender  Sender
	cliWait *sync.WaitGroup
	config  Config
	workers []WorkerConf
}

//Manager is reposible to start a chain
type Manager interface {
	Send(project.Project)
	Close()
	Run(downloader.Manager)
}

//NewManager return a Manager configured
func NewManager(config Config, workers []WorkerConf) (Manager, error) {
	var sender Sender
	var err error
	cliWait := &sync.WaitGroup{}
	if config.Web.Enabled {
		sender, err = NewWorkers(workers, func(p project.Project, err error) {
			if err != nil {
				loggerManager.WithField("project", fmt.Sprintf("%s/%s/%s", p.TypeHost, p.Organisation, p.Name)).WithError(err).Error("Project finish")
				_, err := deleter.Delete(p)
				if err != nil {
					loggerManager.WithField("project", fmt.Sprintf("%s/%s/%s", p.TypeHost, p.Organisation, p.Name)).WithError(err).Error("Can't delete when project in error")
				}
			} else {
				loggerManager.WithField("project", fmt.Sprintf("%s/%s/%s", p.TypeHost, p.Organisation, p.Name)).Info("Project finish")
			}
		})
	} else if config.Cli.Enabled {
		cliWait.Add(1)
		sender, err = NewWorkers(workers, func(p project.Project, err error) {
			defer cliWait.Done()
			if err != nil {
				loggerManager.WithError(err).Error("Error during process")
				os.Exit(1)
			}
		})
	} else {
		return nil, errors.New("You need to specify orchestrator.web=true or orchestrator.cli=true in config file")
	}
	return &manager{
		sender:  sender,
		cliWait: cliWait,
		config:  config,
		workers: workers,
	}, err
}

func (m *manager) Send(p project.Project) {
	m.sender.Send(p)
}

func (m *manager) Close() {
	m.sender.Close()
}

func (m *manager) Run(downloaderManager downloader.Manager) {
	if m.config.Web.Enabled {
		startWeb(m.sender, m.config.Web, downloaderManager)
	} else if m.config.Cli.Enabled {
		m.startCli(m.config.Cli.Path, downloaderManager)
	}
}

func (m *manager) startCli(url string, downloaderManager downloader.Manager) {
	defer m.sender.Close()
	loggerCli.Info("Start")
	err := startCli(url, m.sender, downloaderManager)
	if err != nil {
		loggerManager.WithError(err).Error("Can't initialize")
		os.Exit(1)
	}
	m.cliWait.Wait()
}
