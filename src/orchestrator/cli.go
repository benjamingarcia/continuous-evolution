package orchestrator

import (
	"continuous-evolution/src/downloader"

	"github.com/sirupsen/logrus"
)

//CliConfig is configuration required by cli
type CliConfig struct {
	Enabled bool
	Path    string
}

//DefaultCliConfig is the default configuration required by cli
var DefaultCliConfig = CliConfig{Enabled: false, Path: ""}

var loggerCli = logrus.WithField("logger", "orchestrator/cli")

//StartCli wait during update and exit after, exit 1 if an error is throw
func startCli(url string, sender Sender, downloaderManager downloader.Manager) error {
	project, err := downloaderManager.BuildProject(url)
	if err != nil {
		return err
	}
	sender.Send(project)
	return nil
}
