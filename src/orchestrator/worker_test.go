package orchestrator

import (
	"continuous-evolution/src/project"
	"errors"
	"fmt"
	"testing"
	"time"
)

func TestWorkerFinally(t *testing.T) {
	nbSend := 10000
	outChan := make(chan bool)
	finishChan := make(chan bool)
	go func() {
		after := time.After(5 * time.Second)
		for i := 0; i < nbSend; i++ {
			select {
			case <-outChan:
				fmt.Println("ok")
			case <-after:
				t.Fatal("Should retreive project before 1 second")
			}
		}
		finishChan <- true
	}()
	w, err := newWorker(1, func(p project.Project) (project.Project, error) {
		return p.SetGitURL("gitURL"), nil
	}).
		finally(func(p project.Project, err error) {
			if p.GitURL != "gitURL" {
				t.Fatalf("Should be gitURL isntead of %s", p.GitURL)
			}
			outChan <- true
		})
	if err != nil {
		t.Fatal("Valid shoudl not throw err", err)
	}
	for i := 0; i < nbSend; i++ {
		w.Send(project.Project{})
	}
	<-finishChan
}

func TestWorkerThen(t *testing.T) {
	nbSend := 10000
	outChan := make(chan bool)
	finishChan := make(chan bool)
	go func() {
		after := time.After(5 * time.Second)
		for i := 0; i < nbSend; i++ {
			select {
			case <-outChan:
				fmt.Println("ok")
			case <-after:
				t.Fatal("Should retreive project before 1 second")
			}
		}
		finishChan <- true
	}()
	w := newWorker(1, func(p project.Project) (project.Project, error) {
		return p.SetGitURL("gitURL"), nil
	})
	o, err := w.then(newWorker(1, func(p project.Project) (project.Project, error) {
		return p.SetGitURL("gitURL2"), nil
	}))
	o.finally(func(p project.Project, err error) {
		if p.GitURL != "gitURL2" {
			t.Fatalf("Should be gitURL isntead of %s", p.GitURL)
		}
		outChan <- true
	})
	if err != nil {
		t.Fatal("Valid shoudl not throw err", err)
	}
	for i := 0; i < nbSend; i++ {
		w.Send(project.Project{})
	}
	<-finishChan
}

func TestError(t *testing.T) {
	outChan := make(chan bool)
	finishChan := make(chan bool)
	go func() {
		select {
		case <-outChan:
			fmt.Println("ok")
			finishChan <- true
		case <-time.After(1 * time.Second):
			t.Fatal("Should retreive project before 1 second")
		}
	}()
	w := newWorker(1, func(p project.Project) (project.Project, error) {
		return p, errors.New("fake error")
	})
	o, err := w.then(newWorker(1, func(p project.Project) (project.Project, error) {
		return p.SetGitURL("gitURL2"), nil
	}))
	if err != nil {
		t.Fatal("Valid shoudl not throw err", err)
	}
	o, err = o.finally(func(p project.Project, err error) {
		if err == nil {
			t.Fatalf("Should dispatch error")
		}
		outChan <- true
	})
	w.Send(project.Project{})
	<-finishChan
}

func TestThenAndFinally(t *testing.T) {
	w := newWorker(1, func(p project.Project) (project.Project, error) {
		return p, errors.New("fake error")
	})
	_, err := w.then(newWorker(1, func(p project.Project) (project.Project, error) {
		return p.SetGitURL("gitURL2"), nil
	}))
	w, err = w.finally(func(p project.Project, err error) {})
	if err == nil {
		t.Fatal("Add then and finally should throw err")
	}
}

func TestWorkers(t *testing.T) {
	chanTest := make(chan project.Project)
	workers := []WorkerConf{
		{Active: true, PoolSize: 1, Callback: func(p project.Project) (project.Project, error) { return project.Project{Name: "2"}, nil }},
		{Active: true, PoolSize: 1, Callback: func(p project.Project) (project.Project, error) { return project.Project{Name: "3"}, nil }},
		{Active: true, PoolSize: 1, Callback: func(p project.Project) (project.Project, error) { return project.Project{Name: "4"}, nil }},
	}
	sender, err := NewWorkers(workers, func(p project.Project, err error) {
		if err != nil {
			t.Fatal("No error in chain")
		}
		chanTest <- p
	})
	if err != nil {
		t.Fatal("Good conf should not throw error")
	}
	sender.Send(project.Project{})
	select {
	case p := <-chanTest:
		if p.Name != "4" {
			t.Fatal("All worker should be called")
		}
	case <-time.After(1 * time.Second):
		t.Fatal("Test should finish before 1 second")
	}
}

func TestWorkersError(t *testing.T) {
	chanTest := make(chan error)
	workers := []WorkerConf{
		{Active: true, PoolSize: 1, Callback: func(p project.Project) (project.Project, error) { return project.Project{Name: "2"}, nil }},
		{Active: true, PoolSize: 1, Callback: func(p project.Project) (project.Project, error) { return p, errors.New("fake error") }},
		{Active: true, PoolSize: 1, Callback: func(p project.Project) (project.Project, error) { return project.Project{Name: "4"}, nil }},
	}
	sender, err := NewWorkers(workers, func(p project.Project, err error) {
		if err == nil {
			t.Fatal("Error in chain")
		}
		chanTest <- err
	})
	if err != nil {
		t.Fatal("Good conf should not throw error")
	}
	sender.Send(project.Project{})
	select {
	case <-chanTest:
	case <-time.After(1 * time.Second):
		t.Fatal("Test should finish before 1 second")
	}
}
