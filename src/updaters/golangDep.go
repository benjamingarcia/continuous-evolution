package updaters

import (
	"continuous-evolution/src/project"
	"continuous-evolution/src/tools"
	"encoding/json"
	"os"
	"path"

	"github.com/sirupsen/logrus"
)

//GolangDepConfig is configuration required by golang/dep updater
type GolangDepConfig struct {
	Enabled bool
	Image   string
	Command []string
	Volumes []string
	Env     []string
	DNS     []string
}

//DefaultGolangDepConfig is the default configuration required by golang/dep updater
var DefaultGolangDepConfig = GolangDepConfig{
	Enabled: true,
	Image:   "registry.gitlab.com/continuousevolution/continuous-evolution/golangdep",
	Command: []string{"ensure", "--no-vendor", "--update"},
	Volumes: []string{},
	Env:     []string{},
	DNS:     []string{},
}

var golangDepLogger = logrus.WithField("logger", "updaters/golangDep")

func golangDepIsPackageFile(pathString string, f os.FileInfo) bool {
	return !f.IsDir() && f.Name() == "Gopkg.toml"
}

type golangDepUpdater struct {
	config       GolangDepConfig
	dockerClient tools.Docker
}

func newGolangDep(config Config) Updater {
	return &golangDepUpdater{config: config.Golangdep, dockerClient: tools.NewDocker()}
}

func (u *golangDepUpdater) Update(_ string, pkg project.Package, excludes []string) ([]project.Dependency, error) {
	dirpath := path.Dir(pkg.Path)
	binds := append(u.config.Volumes, dirpath+":/go/src/project")
	dependencies, err := u.status(binds)
	if err != nil {
		golangDepLogger.WithError(err).Error("can't get current status")
		return nil, err
	}
	command := u.config.Command
	if len(excludes) > 0 {
		for _, dep := range dependencies {
			for _, exclude := range excludes {
				if exclude != dep.Name {
					command = append(command, dep.Name)
				}
			}
		}
	}
	u.dockerClient.StartDocker(u.config.Image, command, binds, "/go/src/project", u.config.Env, u.config.DNS)
	return dependencies, nil
}

type jsonDep struct {
	ProjectRoot string
	Revision    string
	Latest      string
}

func (u *golangDepUpdater) status(binds []string) ([]project.Dependency, error) {
	marshaled := u.dockerClient.StartDocker(u.config.Image, []string{"status", "-json"}, binds, "/go/src/project", u.config.Env, u.config.DNS)
	unmarshaled := make([]jsonDep, 0)
	if err := json.Unmarshal(marshaled, &unmarshaled); err != nil {
		return nil, err
	}
	dependencies := make([]project.Dependency, 0)
	for _, dep := range unmarshaled {
		if dep.Revision != dep.Latest {
			dependencies = append(dependencies, project.Dependency{Name: dep.ProjectRoot, OriginalVersion: dep.Revision, NewVersion: dep.Latest, CanBeExcludes: true})
		}
	}
	return dependencies, nil
}
