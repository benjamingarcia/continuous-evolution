package scheduler

import (
	"bufio"
	"context"
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/orchestrator"
	"continuous-evolution/src/project"
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

//Config is configuration required by scheduler
type Config struct {
	Enabled             bool
	PathDb              string
	WaitDurationProcess string
}

//DefaultConfig is the default configuration required by scheduler
var DefaultConfig = Config{Enabled: false, PathDb: "/tmp", WaitDurationProcess: "24h"}

var loggerScheduler = logrus.WithField("logger", "scheduler/scheduler")

type scheduler struct {
	config              Config
	waitDurationProcess time.Duration
	ctxStop             context.Context
	cancelStop          context.CancelFunc
	muProjects          sync.Mutex
	projects            []minimalProject
}

//Scheduler is responsible to schedule update of project stored in config file
//Start to start checking which project need update
//Save(project.project) to update or insert a project
//Close to gracefully kill scheduler
type Scheduler interface {
	Start(orchestrator.Sender, downloader.Manager) error
	Save(project.Project) (project.Project, error)
	Delete(project.Project) error
	Close()
}

//minimal is a sub representation of a project used internally by scheduler to save only properties required
type minimalProject struct {
	GitURL          string    `json:"gitUrl"`
	TypeHost        string    `json:"typeHost"`
	Organisation    string    `json:"organisation"`
	Name            string    `json:"name"`
	CreatedDate     time.Time `json:"createdDate"`
	LastUpdatedDate time.Time `json:"LastUpdatedDate"`
}

//NewScheduler return a scheduler which is responsible to
// * load projects from config file
// * fire an update every duration depending on project.LastUpdatedDate
//Call Start() to start the scheduler
//Call Delete() to remove project
//Call Close() to stop it gracefully
func NewScheduler(config Config) (Scheduler, error) {
	if config.Enabled {
		if config.PathDb == "" {
			return nil, errors.New("You must set a valid pathDb")
		}
		d, err := time.ParseDuration(config.WaitDurationProcess)
		if err != nil {
			return nil, err
		}

		ctx, cancel := context.WithCancel(context.Background())

		sc := &scheduler{
			config:              config,
			waitDurationProcess: d,
			ctxStop:             ctx,
			cancelStop:          cancel,
			projects:            make([]minimalProject, 0),
		}

		fileConfig, err := os.Open(sc.config.PathDb)
		if err != nil {
			return nil, err
		}
		defer fileConfig.Close()
		scanner := bufio.NewScanner(fileConfig)
		scanner.Split(bufio.ScanLines)

		sc.muProjects.Lock()
		defer sc.muProjects.Unlock()

		for scanner.Scan() {
			err := json.Unmarshal(scanner.Bytes(), &sc.projects)
			if err != nil {
				loggerScheduler.WithError(err).Error("Can't unmarshal project in config")
				continue
			}
		}
		return sc, nil
	}
	return &scheduler{}, nil
}

func (s *scheduler) Start(sender orchestrator.Sender, downloaderManager downloader.Manager) error {
	if !s.config.Enabled {
		return errors.New("You can't call start if scheduler is not enabled")
	}
	go func() {
		for {
			found, nextMinimalProject, nextProjectToUpdateDuration := s.nextProjectToUpdate()
			select {
			case <-time.After(nextProjectToUpdateDuration): //wait even if found is false to not while(1) (if no project in file for example)
				if found {
					nextProject, err := downloaderManager.BuildProject(nextMinimalProject.GitURL)
					if err != nil {
						loggerScheduler.WithField("git url", nextMinimalProject.GitURL).WithError(err).Error("Can't pass from minimalProject to project !!??")
						break
					}
					s.Save(nextProject)
					sender.Send(nextProject)
				}
			case <-s.ctxStop.Done():
				return
			}
		}
	}()
	return nil
}

func (s *scheduler) nextProjectToUpdate() (bool, minimalProject, time.Duration) {
	s.muProjects.Lock()
	defer s.muProjects.Unlock()
	nextProjectToUpdateDuration := 1 * time.Minute //if no project present we wait 1 minute
	var nextProject minimalProject
	found := false
	for _, project := range s.projects {
		nextProjectDuration := s.waitDurationProcess - time.Since(project.LastUpdatedDate)
		if !found || nextProjectDuration < nextProjectToUpdateDuration {
			nextProjectToUpdateDuration = nextProjectDuration
			nextProject = project
			found = true
		}
	}
	return found, nextProject, nextProjectToUpdateDuration
}

//Save search for project url in projects, if found update lastUpdatedDate else add new project
func (s *scheduler) Save(originalProject project.Project) (project.Project, error) {
	if !s.config.Enabled {
		return originalProject, errors.New("You can't call Save if scheduler is not enabled")
	}
	project := originalProject.UpdateLastUpdatedDate()
	s.muProjects.Lock()
	defer s.muProjects.Unlock()
	found := false
	miniProject := minimalProject{
		GitURL:          project.GitURL,
		TypeHost:        project.TypeHost,
		Organisation:    project.Organisation,
		Name:            project.Name,
		CreatedDate:     project.CreatedDate,
		LastUpdatedDate: project.LastUpdatedDate,
	}
	for index, savedProject := range s.projects {
		if project.IsSame(savedProject.TypeHost, savedProject.Organisation, savedProject.Name) {
			s.projects[index] = miniProject
			found = true
			break
		}
	}
	if !found {
		s.projects = append(s.projects, miniProject)
	}

	err := s.saveProjectsToFile()

	return project, err
}

func (s *scheduler) saveProjectsToFile() error {
	if s.config.PathDb != "" {
		json, err := json.Marshal(s.projects)
		if err != nil {
			return err
		}
		err = ioutil.WriteFile(s.config.PathDb, json, 0644)
		if err != nil {
			return err
		}
	}
	return nil
}

func (s *scheduler) Delete(p project.Project) error {
	if !s.config.Enabled {
		return errors.New("You can't call Delete if scheduler is not enabled")
	}
	s.muProjects.Lock()
	defer s.muProjects.Unlock()
	newProjects := make([]minimalProject, 0)
	found := false
	for _, savedProject := range s.projects {
		if !p.IsSame(savedProject.TypeHost, savedProject.Organisation, savedProject.Name) {
			found = true
			newProjects = append(newProjects, savedProject)
		}
	}
	if found { //if not found no need to update (can arrived when a new project contains error)
		s.projects = newProjects
		return s.saveProjectsToFile()
	}
	return nil
}

func (s *scheduler) Close() {
	if !s.config.Enabled {
		return
	}
	s.cancelStop()
}
