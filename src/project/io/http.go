package io

import (
	"bytes"
	"context"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
)

var loggerHTTP = logrus.WithField("logger", "project/io/http")

//HTTP is the struct to make http call with project
type HTTP struct {
	login         string
	token         string
	requestFabric func(method, url string, body io.Reader) (*http.Request, error)
	do            func(req *http.Request) (*http.Response, error)
}

//NewHTTP return an instance to make http call with project
func NewHTTP(login string, token string) *HTTP {
	client := &http.Client{}
	return &HTTP{
		login:         login,
		token:         token,
		requestFabric: http.NewRequest,
		do:            client.Do,
	}
}

func (h *HTTP) send(req *http.Request, url string) ([]byte, error) {
	req.Header.Set("Content-Type", "application/json")
	req.SetBasicAuth(h.login, h.token)

	ctx, cancel := context.WithTimeout(context.Background(), 1*time.Minute)
	defer cancel()
	resp, err := h.do(req.WithContext(ctx))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	loggerHTTP.WithFields(logrus.Fields{
		"status":  resp.Status,
		"headers": resp.Header,
	}).Info("Response from host")
	bodyRet, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		loggerHTTP.WithField("body", string(bodyRet)).WithError(err).Error("Body from host")
		return nil, err
	}
	return bodyRet, nil
}

//Get make an http.Get with BasicAuth of project.Login, project.Token
func (h *HTTP) Get(url string) ([]byte, error) {
	req, err := h.requestFabric("GET", url, nil)
	if err != nil {
		return nil, err
	}
	return h.send(req, url)
}

//Post make an http.Post with BasicAuth of project.Login, project.Token
func (h *HTTP) Post(url string, body []byte) ([]byte, error) {
	req, err := h.requestFabric("POST", url, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	return h.send(req, url)
}

//Patch make an http.Patch with BasicAuth of project.Login, project.Token
func (h *HTTP) Patch(url string, body []byte) ([]byte, error) {
	req, err := h.requestFabric("PATCH", url, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	return h.send(req, url)
}

//Put make an http.Put with BasicAuth of project.Login, project.Token
func (h *HTTP) Put(url string, body []byte) ([]byte, error) {
	req, err := h.requestFabric("PUT", url, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}
	return h.send(req, url)
}
