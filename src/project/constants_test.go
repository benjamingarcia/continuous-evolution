package project

import (
	"bytes"
	"fmt"
	"html/template"
	"testing"

	"github.com/sergi/go-diff/diffmatchpatch"
)

const data = `New dependencies versions :

* [x] pakage/Dockerfile :
    * [x] depthName : ` + "`1.0.0`" + `
    * [ ] other : 0.0.1 => ` + "`0.2.0`" + `

* [x] pakage/pom.xml :
    * depthName2 : ` + "`3.0.0`" + `

* [ ] pakage/noDepth

[Relaunch update](http://test.com)

> This merge request is generate, no human has tested the resulting code. If you are confident with your CI accept it, else fetch the result and test it manually. By the way adding CI is always a good idea ;)

Gracefully updated by [ContinuousEvolution](https://gitlab.com/ContinuousEvolution/continuous-evolution) with :heart:

> If you close this merge-request, ContinuousEvolution will remove this project from it database. No more update will be delivered until you resubscribe.`

func TestFetchLatest(t *testing.T) {
	excludes := make(map[string][]string)
	excludes["pakage/noDepth"] = []string{}
	excludes["pakage/Dockerfile"] = []string{"other"}
	p := Project{
		Name:         "test",
		URLReProcess: "http://test.com",
		PathToWrite:  "/tmp",
		Packages: []Package{
			{Path: "/tmp/test/pakage/Dockerfile", UpdatedDependencies: []Dependency{
				{Name: "depthName", NewVersion: "1.0.0", CanBeExcludes: true},
				{Name: "other", NewVersion: "0.2.0", OriginalVersion: "0.0.1", CanBeExcludes: true},
			}},
			{Path: "/tmp/test/pakage/pom.xml", UpdatedDependencies: []Dependency{{Name: "depthName2", NewVersion: "3.0.0", CanBeExcludes: false}}},
			{Path: "/tmp/test/pakage/noDepth"},
		},
		Excludes: excludes,
	}
	tplMergeRequestBody, err := template.New("MergeRequest").Parse(MergeRequestBody)
	if err != nil {
		t.Fatal("Good template should not throw error ", err)
	}
	var mergeRequestBody bytes.Buffer
	err = tplMergeRequestBody.Execute(&mergeRequestBody, p)
	if err != nil {
		t.Fatal("Good merge-request should not throw error ", err)
	}

	if mergeRequestBody.String() != data {
		dmp := diffmatchpatch.New()
		diffs := dmp.DiffMain(data, mergeRequestBody.String(), true)
		fmt.Println(dmp.DiffPrettyText(diffs))

		fmt.Println(mergeRequestBody.String())

		t.Fatal("Merge request template error")
	}

}
