package main

import (
	"continuous-evolution/src/downloader"
	"continuous-evolution/src/orchestrator"
	"continuous-evolution/src/reporters"
	"continuous-evolution/src/scheduler"
	"continuous-evolution/src/updaters"

	toml "github.com/pelletier/go-toml"
)

//Config required by Continuous-evolution to work
type Config struct {
	Orchestrator orchestrator.Config
	Downloader   downloader.Config
	Updater      updaters.Config
	Reporter     reporters.Config
	Scheduler    scheduler.Config
}

//GetConfig load config from file if configPath is not empty else return default config
func GetConfig(configPath string) (Config, error) {
	conf := Config{
		Orchestrator: orchestrator.DefaultConfig,
		Downloader:   downloader.DefaultConfig,
		Updater:      updaters.DefaultConfig,
		Reporter:     reporters.DefaultConfig,
		Scheduler:    scheduler.DefaultConfig,
	}

	if configPath == "" {
		return conf, nil
	}

	tree, err := toml.LoadFile(configPath)
	if err != nil {
		return conf, err
	}

	if err := tree.Unmarshal(&conf); err != nil {
		return conf, err
	}

	return conf, nil
}
