#!/usr/bin/env bash

name=$(date +%s)
path="/tmp/coverage-continuousevolution-$name.cov"

generate() {
    go test -coverpkg=./src/... -covermode=count -short -coverprofile="$path" ./src/...
}

displayTerminal() {
    go tool cover -func "$path"
}

displayHtml() {
    go tool cover -html "$path"
}

usage () {
    cat << EOF

Description: Go coverage for all packages.

Usage: resources/scripts/cover.sh [COMMAND]

Commands:

-t | -terminal			Prompt the list of go packages with percent coverage.
-m | -html			Open default web browser with percent coverage.
-h | -help			Display this help

EOF

}

if [[ -z $1 ]]; then
    echo "Error : command empty"
    usage
    exit 1
fi

if [[ "$1" == "-help" || "$1" == "-h" ]]; then
    usage
    exit 0
fi

generate
if [[ "$1" == "-terminal" || "$1" == "-t" ]]; then
    displayTerminal; else
    displayHtml
fi