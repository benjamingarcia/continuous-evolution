#!/usr/bin/env bash

installDepthAndLaunch() {
    if hash hugo 2>/dev/null; then
        echo "hugo found no need to install"
    else
        echo "install hugo"
        go get -u -v github.com/gohugoio/hugo
    fi

    if hash godocdown 2>/dev/null; then
        echo "godocdown found no need to install"
    else
        echo "install godocdown"
        go get github.com/robertkrimen/godocdown/godocdown
    fi

    if hash minify 2>/dev/null; then
        echo "minify found no need to install"
    else
        echo "install minify"
        go get github.com/tdewolff/minify/cmd/minify
    fi

    goDoc

    if [[ "$1" == "-dev" ]]; then
        env HUGO_BASEURL="http://127.0.0.1:1313/" hugo server -wDs doc --disableFastRender
    else
        hugo -s doc
        minify --recursive --output ./doc/public_min/ ./doc/public
        cp -u -r ./doc/public/images/. ./doc/public_min/images
        cp -u -r ./doc/public/font/. ./doc/public_min/font
    fi
}

goDoc() {
    echo -e "---\ntitle: \"GoDoc\"\nmenu: \n    main:\n        weight: 21\n        parent: \"Develop\"\nsubnav: \"true\"\ndescription: \"All documentations\"\n---\n" > doc/content/godoc.md
    for p in `go list ./src/...`
    do
        godocdown ${p} >> doc/content/godoc.md
    done
}

###
# $1 file will be copied
# $2 file where copy
# $3 title of generated page
# $4 weight of the menu (bigger is right)
# $5 subnav "true" or "false"
# $6 description of page
# $7 icon for menu
# $8 parent menu or empty
###
addFile() {
    cp $1 $2
    sed -i -E "s/doc\/static\/images\/([^\)]+)/\/images\/\1/g" $2
    if [ -z "$8" ]
        then
            echo -e "---\ntitle: \"$3\"\nmenu: \n    main:\n        weight: $4\n        pre: '<i class=\"fa fa-$7\" aria-hidden=\"true\"></i>'\nsubnav: \"$5\"\ndescription: \"$6\"\n---\n$(cat $2)" > $2
    else
        echo -e "---\ntitle: \"$3\"\nmenu: \n    main:\n        weight: $4\n        pre: '<i class=\"fa fa-$7\" aria-hidden=\"true\"></i>'\n        parent: \"$8\"\nsubnav: \"$5\"\ndescription: \"$6\"\n---\n$(cat $2)" > $2
    fi
    
}

if [ ! -d "doc/themes/hugorha" ]; then
    mkdir -p doc/themes/hugorha
    git clone https://github.com/itkSource/hugorha.git doc/themes/hugorha
fi

clean() {
    rm doc/content/README.md
    rm doc/content/CHANGELOG.md
    rm doc/content/CONTRIBUTING.md
    rm doc/content/LICENCE.md
    rm doc/content/godoc.md
}

addFile README.md doc/content/README.md "Readme" 3 "true" "General information" "book" "Develop"
addFile CHANGELOG.md doc/content/CHANGELOG.md "Changelog" 10 "true" "All versioned features" "calendar-o"
addFile CONTRIBUTING.md doc/content/CONTRIBUTING.md "Contributing" 20 "true" "How to contribute ?" "code-fork" "Develop"
addFile LICENCE.md doc/content/LICENCE.md "Licence" 30 "false" "Legal information" "linux"

trap 'clean' 2 3

installDepthAndLaunch $1

if [[ "$1" != "-dev" ]]; then
    clean
fi
