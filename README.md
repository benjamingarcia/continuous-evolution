# Continuous Evolution

> Update your dependencies with (merge | pull)-request.

## Features

### Download

ContinuousEvolution get your code source from git. All git repository (github, gitlab...) should work. It can also retreive your code source from an absolute path, useful for the cli binary.

### Update

|  Package Manager   |       File       |  Update  |           Tool              |  Exclude  |Updated Pkg |SemverRestriction| Changelog  |
|--------------------|------------------|----------|-----------------------------|-----------|------------|-----------------|------------|
|[npm][1]            |   package.json   |    ✔     |      [creack/ncu][1.1]      |     ✔     |     ✔      |         ✘       |     ✘      |
|[docker][2]         |    Dockerfile    |    ✔     |       [internal][2.1]       |     ✔     |     ✔      |         ✘       |     ✘      |
|[docker-compose][10]|docker-compose.yml|    ✔     |       [internal][10.1]      |     ✔     |     ✔      |         ✘       |     ✘      |
|[golang/dep][7]     |    Gopkg.toml    |    ✔     |      [golang/dep][7]        |     ✔     |     ✔      |         ✘       |     ✘      |
|[gradle][5]         | settings.gradle  |    ✔     |[gradle-versions-plugin][5.1]|     ✔     |     ✔      |         ✘       |     ✘      |
|[sbt][11]           |    build.sbt     |    ✔     |     [sbt-updates][11.1]     |     ✔     |     ✔      |         ✘       |     ✘      |
|[maven][4]          |      pom.xml     |    ✔     |        [maven][4]           |     ✔     | [#24][4.1] |         ✘       |     ✘      |
|[pip][3]            | requirements.txt |    ✔     |[chauffer/pip3-compile][3.1] | [#13][3.2]| [#25][3.3] |         ✘       |     ✘      |
|[cargo][6]          |        ✘         |[#21][6.1]|             ✘               |     ✘     |     ✘      |         ✘       |     ✘      |
|[glide][8]          |        ✘         |    ✘     |             ✘               |     ✘     |     ✘      |         ✘       |     ✘      |
|[godep][9]          |        ✘         |[#23][9.1]|             ✘               |     ✘     |     ✘      |         ✘       |     ✘      |

[1]:https://www.npmjs.com
[1.1]:https://github.com/tjunnone/npm-check-updates
[2]:https://www.docker.com
[2.1]:https://gitlab.com/ContinuousEvolution/continuous-evolution/blob/master/src/updaters/docker.go
[10]:https://docs.docker.com/compose/
[10.1]:https://gitlab.com/ContinuousEvolution/continuous-evolution/blob/master/src/updaters/dockerCompose.go
[3]:https://pypi.python.org/pypi/pip
[3.1]:https://hub.docker.com/r/chauffer/pip3-compile/
[3.2]:https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/13
[3.3]:https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/25
[4]:https://maven.apache.org
[4.1]:https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/24
[5]:https://gradle.org
[5.1]:https://github.com/ben-manes/gradle-versions-plugin
[6]:https://crates.io
[6.1]:https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/21
[7]:https://github.com/golang/dep
[8]:https://glide.sh
[9]:https://github.com/tools/godep
[9.1]:https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/23
[11]:https://www.scala-sbt.org/
[11.1]:https://github.com/rtimush/sbt-updates

#### Legend

* Package Manager : the tool wich manage your dependencies, ContinuuousEvolution will update its files.
* File : ContinuousEvolution will update this files if new dependencies exists
* Update : ContinuousEvolution can update dependencies
* Tool : the tool used to update dependencies
* Excude : ContinuousEvolution can exclude dependencies during update process
* Updated Pkg : ContinuousEvolution can report what is updated in merge-request, else only diff can show new dependencies
* SemverRestriction : ContinuousEvolution can update dependencies depending on what you want : latest, major, minor, bug...
* Changelog : ContinuousEvolution can add a link to the changelog of the newly updated dependency

### Report

ContinuousEvolution can report the new dependencies by :

* Create a branch `UpdateLibs`, commit new dependencies and push to origin
* Create a github pull-request if your git url is hosted on github
* Else create a gitlab merge-request, check if the url is a gitlab instance is not implemented [#27](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues/27)

## Links

* [Issues](https://gitlab.com/ContinuousEvolution/continuous-evolution/issues), please be sure to have read the [contributing](https://continuousevolution.gitlab.io/continuous-evolution/contributing/#reporting-bugs) page before.
* [Quickstart](https://continuousevolution.gitlab.io/continuous-evolution/#tryit) to use ContinuousEvolution
* [Develop](https://continuousevolution.gitlab.io/continuous-evolution/env) to add features on ContinuousEvolution, please be sure to have read the [contributing](https://continuousevolution.gitlab.io/continuous-evolution/contributing/#code-contribution) page before.

## Contribution

This repository ❤ merge-request ;) Make sure no one else work on same feature by opening an issue!

## License

This project is licensed under the AGPL License - see the `LICENSE.md` file for details.
